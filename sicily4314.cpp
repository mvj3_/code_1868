//题目分析:水题可耻，刷水题的我更可耻→ →

//题目网址:http://soj.me/4134

#include <iostream>
#include <cmath>

using namespace std;

bool isPrime(int p)
{
    if (p == 1 || p == 0) {
        return false;
    } else {
        for (int i = 2; i <= sqrt(double(p)); i++) {
            if (p%i == 0)
                return false;
        }
    }
    return true;
}

int gcd(int x, int y)
{
    if(x%y == 0)
        return y;
    return gcd(y, x%y);
}

int main()
{
    int cases;
    int a, b;
    cin >> cases;
    while (cases--) {
        cin >> a >> b;
        if (isPrime(a) && isPrime(b)) {
            cout << "Yes" << endl;
        } else if (gcd(a, b) == 1) {
            cout << "Yes" << endl;
        } else {
            cout << "No" << endl;
        }
    }
    return 0;
}                                 